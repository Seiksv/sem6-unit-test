//
//  TestNetworking.swift
//  sem6 maooTests
//
//  Created by Seiks on 13/3/22.
//

import XCTest

@testable import sem6_maoo
class TestNetworking: XCTestCase {

    func testGetAmiiboSeries() {
        let path = "/api/amiiboseries"
        let getGameSeries = APIRouter.getAmiboSeries
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.path , path)
        XCTAssertEqual(try! getGameSeries.asURLRequest().httpMethod , "GET")
    }

    func testGetGameSeries() {
        let path = "/api/gameseries"
        let getGameSeries = APIRouter.getGameSeries
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.path , path)
        XCTAssertEqual(try! getGameSeries.asURLRequest().httpMethod , "GET")
    }
    
    func testGetGameSeriesWithKey() {
        let path = "/api/amiibo"
        let getGameSeries = APIRouter.getAmibosGameSeries(id: "0x10")
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.path , path)
        XCTAssertEqual(try! getGameSeries.asURLRequest().httpMethod , "GET")
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.query , "gameseries=0x10")
    }
    
    func testGetAmiiboSeriesWithKey() {
        let path = "/api/amiibo"
        let getGameSeries = APIRouter.getAmibosSeries(id: "0x10")
      
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.path , path)
        XCTAssertEqual(try! getGameSeries.asURLRequest().httpMethod , "GET")
        XCTAssertEqual(try! getGameSeries.asURLRequest().url?.query , "amiiboseries=0x10")
        
    }
    
}
