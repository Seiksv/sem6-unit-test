//
//  testCacheGameSeries.swift
//  sem6 maooTests
//
//  Created by Seiks on 13/3/22.
//

import XCTest
@testable import sem6_maoo
class TestCacheGameSeries: XCTestCase {
    
    func testSaveSeriesCache() {
        
        let seriesManager = SeriesCacheManager(cacheKey: "amiibogameseriesTEST")
        var testAmiiboArray: [Series] = []
        let testAmiiboValue: Series = .init(key: "0x00test", name: "test")
        testAmiiboArray.append(testAmiiboValue)
        XCTAssertNotNil(try seriesManager.set(series: testAmiiboArray))
        
    }
    
    func testGetSeriesCache() {
        
        let seriesManager = SeriesCacheManager(cacheKey: "amiibogameseriesTEST")
        let testAmiiboValue: Series = .init(key: "0x00test", name: "test")
        var testAmiiboArray: [Series] = []
        testAmiiboArray.append(testAmiiboValue)
        
        XCTAssertEqual(seriesManager.getEpisodes(), testAmiiboArray)
    }
    
    func testSaveSeriesDetailCache() {
        
        let seriesManager = SeriesDetailCacheManager(cacheKey: "amiiboseriesdetailTEST")
        let testAmiiboValue: SeriesDetail = .init(amiiboSeries: "test", gameSeries: "test", character: "test", head: "test", image: "test", name: "test", release: ReleasesModel.init(au: "test", eu: "test", jp: "test", na: "test"), tail: "test", type: "test")
        var testAmiiboArray: [SeriesDetail] = []
        testAmiiboArray.append(testAmiiboValue)
       
        XCTAssertNotNil(try seriesManager.set(series: testAmiiboArray))
        
    }
    
    func testGetSeriesDetailCache() {
        
        let seriesManager = SeriesDetailCacheManager(cacheKey: "amiiboseriesdetailTEST")
        let testAmiiboValue: SeriesDetail = .init(amiiboSeries: "test", gameSeries: "test", character: "test", head: "test", image: "test", name: "test", release: ReleasesModel.init(au: "test", eu: "test", jp: "test", na: "test"), tail: "test", type: "test")
        var testAmiiboArray: [SeriesDetail] = []
        testAmiiboArray.append(testAmiiboValue)
        
        XCTAssertEqual(seriesManager.getSeries(), testAmiiboArray)
    }
}
