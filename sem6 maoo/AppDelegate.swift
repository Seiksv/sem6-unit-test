//
//  AppDelegate.swift
//  sem6 maoo
//
//  Created by Seiks on 9/3/22.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                     launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureAppearance()
        
        return true
    }
    
    func configureAppearance() {
        UISearchBar.appearance().tintColor = .brown
        UINavigationBar.appearance().tintColor = .blue
    }
}

