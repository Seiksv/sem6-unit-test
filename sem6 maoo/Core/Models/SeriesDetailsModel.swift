//
//  SeriesDetailsModel.swift
//  sem6 maoo
//
//  Created by Seiks on 12/3/22.
//

import Foundation
public struct SeriesDetail: Equatable {
    
        // MARK: - Properties
    public let amiiboSeries: String?
    public let gameSeries: String?
    public let character: String?
    public let head: String?
    public let image: String?
    public let name: String?
    public let release: ReleasesModel?
    public let tail: String?
    public let type: String?
    
}

public struct AmiiboSeries: Codable {
    
    public let seriesDetail: [SeriesDetail]
    
    enum CodingKeys: String, CodingKey {
        case seriesDetail = "amiibo"
    }
}

extension SeriesDetail: Codable {
    enum CodingKeys: String, CodingKey {
        case amiiboSeries = "amiiboSeries"
        case character = "character"
        case gameSeries = "gameSeries"
        case head = "head"
        case image = "image"
        case name = "name"
        case release = "release"
        case tail = "tail"
        case type = "type"
    }
}
