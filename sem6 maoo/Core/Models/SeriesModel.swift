
    //
    //  SeriesModel.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 11/3/22.
    //

import Foundation
public struct Series: Equatable {
    
        // MARK: - Properties
    public let key: String?
    public let name: String?
}
public struct Amiibo: Codable {
    
    
    public let amiibo: [Series]
    
    enum CodingKeys: String, CodingKey {
        case amiibo = "amiibo"
    }
}

extension Series: Codable {
    enum CodingKeys: String, CodingKey {
        case key = "key"
        case name = "name"
    }
}
