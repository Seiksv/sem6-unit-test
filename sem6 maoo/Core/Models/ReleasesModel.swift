//
//  ReleasesModel.swift
//  sem6 maoo
//
//  Created by Seiks on 12/3/22.
//

import Foundation
public struct ReleasesModel: Equatable {
    
        // MARK: - Properties
    public let au: String?
    public let eu: String?
    public let jp: String?
    public let na: String?
    
}

extension ReleasesModel: Codable {
    enum CodingKeys: String, CodingKey {
        case au = "au"
        case eu = "eu"
        case jp = "jp"
        case na = "na"
    }
}
