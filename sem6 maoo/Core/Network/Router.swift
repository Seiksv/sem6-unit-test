//
//  Router.swift
//  sem6 maoo
//
//  Created by Seiks on 11/3/22.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {

    case getGameSeries
    case getAmiboSeries
    case getAmibosGameSeries(id: String)
    case getAmibosSeries(id: String)
    
    
    private var method: HTTPMethod {
        switch self {
            case .getGameSeries:
                return .get
            case .getAmiboSeries:
                return .get
            case .getAmibosGameSeries(id: _):
                return .get
            case .getAmibosSeries(id: _):
                return .get
            }
    }
    
    private var path: String {
        switch self {
            case .getGameSeries:
                return "api/gameseries"
            case .getAmiboSeries:
                return "api/amiiboseries"
            case .getAmibosGameSeries(id: _):
                return "api/amiibo/"
            case .getAmibosSeries(id: _):
                return "api/amiibo/"
            }
    }
    
    private var parameters: Parameters? {
        switch self {
        case .getAmibosGameSeries(id: let id):
            return ["gameseries": id]
        case .getAmibosSeries(id: let id):
            return ["amiiboseries": id]
        default:
            return nil
        }
    }
    
    private var body: Parameters? {
        switch self {
            default:
                return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try NetworkConstants.baseURL.asURL()
        
            // Construccion del path
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
            // Metodo HTTP (Get, Post, etc.)
        urlRequest.httpMethod = method.rawValue
        
            // Headers de la peticion
        urlRequest.setValue(NetworkConstants.HTTPHeaderFieldValue.json.rawValue,
                            forHTTPHeaderField: NetworkConstants.HTTPHeaderFieldKey.acceptType.rawValue)
        urlRequest.setValue(NetworkConstants.HTTPHeaderFieldValue.json.rawValue,
                            forHTTPHeaderField: NetworkConstants.HTTPHeaderFieldKey.contentType.rawValue)
        
            // Aca se agregan los parametros de GET
        if let parameters = parameters {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
