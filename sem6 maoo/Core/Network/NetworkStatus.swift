//
//  NetworkStatus.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import Reachability

class NetworkStatus {
    
    let reachability = try! Reachability()
    var isConnected = true
    var controller: UIViewController
    
    init(controllerP: UIViewController){
        self.controller = controllerP
    }
    
    func buildNotifier(){
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection == .wifi || reachability.connection == .cellular {
            isConnected = true
            Toast.show(message: "Existe conexion", controller: controller)
        } else {
            isConnected = false
            Toast.show(message: "No conexion", controller: controller)
        }
    }
    
    func getConectionStatus() -> Bool{
        if reachability.connection != .unavailable {
            isConnected = true
            return true
        } else {
            isConnected = false
            return false
        }
    }
}
