    //
    //  Network.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 11/3/22.
    //

import Foundation
import PromiseKit
import Alamofire


extension Session {
        /// Triggers an HTTPRequest using alamofire with a promise as a return type
    func request<T: Codable>(_ urlConvertible: APIRouter) -> Promise<T> {
        return Promise<T> { seal in
                // Trigger∫    e HTTPRequest using Alamofire
            request(urlConvertible).responseDecodable { (response: DataResponse<T, AFError>) in
                    // Check result from response and map it the the promise
                print("\(response)")
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure:
                        // If it's a failure, check status code and map it to my error
                    switch response.response?.statusCode {
                    case 400:
                        seal.reject(MyError.badAPIRequest)
                    case 401:
                        seal.reject(MyError.unauthorized)
                    default:
                        guard NetworkReachabilityManager()?.isReachable ?? false else {
                            seal.reject(MyError.noInternet)
                            return
                        }
                        seal.reject(MyError.unknown)
                    }
                }
            }
        }
    }
}
