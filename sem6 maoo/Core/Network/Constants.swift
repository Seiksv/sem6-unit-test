//
//  Constants.swift
//  sem6 maoo
//
//  Created by Seiks on 11/3/22.
//

import Foundation
struct NetworkConstants {
    
        // Base URL
    static let baseURL = "https://www.amiiboapi.com/"
    
        //Headers del request
    enum HTTPHeaderFieldKey: String {
        case authorization = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
        //Tipo de formato
    enum HTTPHeaderFieldValue: String {
        case json = "application/json"
    }
}
