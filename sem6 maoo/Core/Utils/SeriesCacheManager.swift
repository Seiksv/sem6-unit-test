//
//  SeriesCacheManager.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import Reachability
import CodableCache

final class SeriesCacheManager {
    
    let cache: CodableCache<[Series]>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<[Series]>(key: cacheKey)
    }
    
    func getEpisodes() -> [Series]? {
        return cache.get()
    }
    
    func set(series: [Series]) throws {
        try cache.set(value: series)
    }
}
