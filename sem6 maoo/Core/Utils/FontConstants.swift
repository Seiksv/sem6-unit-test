//
//  FontConstants.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
enum FontConstants: String{
   case helveticaFont = "HelveticaNeue-Bold"
   case hiraginoFont = "Hiragino Sans"
}
