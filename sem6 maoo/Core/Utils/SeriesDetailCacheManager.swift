    //
    //  SeriesCacheManager.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 13/3/22.
    //

import Foundation
import Reachability
import CodableCache

final class SeriesDetailCacheManager {
    
    let cache: CodableCache<[SeriesDetail]>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<[SeriesDetail]>(key: cacheKey)
    }
    
    func getSeries() -> [SeriesDetail]? {
        return cache.get()
    }
    
    func set(series: [SeriesDetail]) throws {
        try cache.set(value: series)
    }
}
