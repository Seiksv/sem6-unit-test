//
//  ErrorHandler.swift
//  sem6 maoo
//
//  Created by Seiks on 11/3/22.
//

import Foundation
public enum MyError: Error {
    
        // MARK: - Internal errors
    case noInternet
    
        // MARK: - API errors
    case badAPIRequest
    
        // MARK: - Auth errors
    case unauthorized
    
        // MARK: - Unknown errors
    case unknown
}
