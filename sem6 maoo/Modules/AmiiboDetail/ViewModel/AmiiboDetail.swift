//
//  AmiiboDetail.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import UIKit
import PromiseKit

class AmiiboDetail: UIViewController  {
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var episodeImage = UIImageView()
    var dataInfo: SeriesDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = dataInfo {
            title = data.name
            createContenScrollView ()
            episodeImage =  UIImageView().createImageView(container: stackView)
            setDataInDetailView(data: data)
        }
    }
    
    func createContenScrollView () {
        view.addSubview(scrollView)
        scrollView.pin(to: view)
        scrollView.alwaysBounceVertical = true
        scrollView.automaticallyAdjustsScrollIndicatorInsets = true
        stackView = UIStackView().createStackView(distribution: .fill, axis: .vertical, spacing: 16)
        scrollView.addSubview(stackView)
        stackView.pinWithMargin(to: scrollView)
        stackView.widthAnchor.constraint(equalToConstant: view.bounds.width - 80).isActive = true
        stackView.isLayoutMarginsRelativeArrangement = true
    }
    
    func setDataInDetailView(data: SeriesDetail){
        
        let imageHelper = ImageHelper()
        //activityLoader.startAnimating()
        if let image = data.image {
            
            let imageProcess = imageHelper.getImage(imageURL: URL(string: image)!)
            imageProcess
                .then({ proccessingData -> Promise<UIImage> in
                    //self.activityLoader.stopAnimating()
                    return Promise.value(proccessingData)
                })
                .done({ [weak self] icon in
                    self?.episodeImage.image = icon
                })
                .catch({ error in
                    print(error)
                })
        } else {
            episodeImage.image = UIImage(named: "noImage")
            //Toast.show(message: StringConstants.noImage, controller: self)
            //print(StringConstants.noImage)
        }
        
        stackView.addArrangedSubview(episodeImage)
        episodeImage.pin(to: stackView)
        
        let amiiboSeriesLabel = UILabel().createLabel(title: data.amiiboSeries!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let characterLabel = UILabel().createLabel(title: data.character!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let gameSeriesLabel = UILabel().createLabel(title: data.gameSeries!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let head = UILabel().createLabel(title: data.head!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let name = UILabel().createLabel(title: data.name!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let tail = UILabel().createLabel(title: data.tail!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        let type = UILabel().createLabel(title: data.type!, height: 22, tag: 0, size: 17, font: FontConstants.hiraginoFont.rawValue, align: .left)
        
        stackView.addArrangedSubview(amiiboSeriesLabel)
        stackView.addArrangedSubview(characterLabel)
        stackView.addArrangedSubview(gameSeriesLabel)
        stackView.addArrangedSubview(head)
        stackView.addArrangedSubview(name)
        stackView.addArrangedSubview(tail)
        stackView.addArrangedSubview(type)
        
        let whiteSpace = UILabel().createLabel(title: " ", height: 20, tag: 0, size: 12, font: FontConstants.hiraginoFont.rawValue, align: .center)
        stackView.addArrangedSubview(whiteSpace)
       
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
}
