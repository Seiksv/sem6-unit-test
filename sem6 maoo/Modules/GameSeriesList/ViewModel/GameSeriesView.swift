//
//  GameSeriesView.swift
//  sem6 maoo
//
//  Created by Seiks on 9/3/22.
//

import Foundation
import UIKit
import PromiseKit
import Alamofire

class GameSeriesView: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var amiiboSeries: [Series] = []
    var filteredSeries: [Series] = []
    var networkStatus: NetworkStatus? = nil
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true }
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkStatus = NetworkStatus(controllerP: self)
        networkStatus?.buildNotifier()
        
        fetchData()
        configSearch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let series: Series
        
        guard
            segue.identifier == "ShowDetailSegue",
            let indexPath = tableView.indexPathForSelectedRow,
            let detailViewController = segue.destination as? DetailView
        else {
            return
        }
        
        if isFiltering {
            series = filteredSeries[indexPath.row]
        } else {
            series = filteredSeries[indexPath.row]
        }
        detailViewController.series = series
        detailViewController.amiiboType = .amiiboserie
    }
    
    func fetchData(){
        let seriesManager = SeriesCacheManager(cacheKey: "amiibogameseries")
        
        if let network = networkStatus, network.getConectionStatus() {
            let session = Session.default
            session.request(.getGameSeries)
                .done { (payload: Amiibo) in
                    print(payload.amiibo)
                    self.filteredSeries = payload.amiibo
                    self.tableView.dataSource = self
                    self.amiiboSeries = payload.amiibo
                    self.tableView.reloadData()
                    try? seriesManager.set(series: payload.amiibo)
                }.catch { error in
                    print(error)
                }
                .finally {
                    print("done")
                }
        } else {
            print("No internet ")
            if let serieCache = seriesManager.getEpisodes()  {
                    //loader.stopAnimating()
                self.filteredSeries = serieCache
                self.tableView.dataSource = self
                self.amiiboSeries = serieCache
                self.tableView.reloadData()
                
                Toast.show(message: "Contenido cargado desde cache", controller: self)
            } else {
                Toast.show(message: "No hay contenido guardado desde cache", controller: self)
            }
        }
    }
    
    func configSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Series"
        navigationItem.searchController = self.searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredSeries = amiiboSeries.filter { (series: Series) -> Bool in
            if isSearchBarEmpty {
                return true
            } else {
                return  series.name!.lowercased().contains(searchText.lowercased())
            }
        }
        tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
}

extension GameSeriesView: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            searchFooter.setIsFilteringToShow(filteredItemCount:
                                                filteredSeries.count, of: amiiboSeries.count)
            return filteredSeries.count
        }
        
        searchFooter.setNotFiltering()
        return amiiboSeries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let amiibo: Series
        
        if isFiltering {
            amiibo = filteredSeries[indexPath.row]
        } else {
            amiibo = amiiboSeries[indexPath.row]
        }
        
        print("\(amiibo)")
        cell.textLabel?.text = amiibo.name
        return cell
    }
}

extension GameSeriesView: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

extension GameSeriesView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}


