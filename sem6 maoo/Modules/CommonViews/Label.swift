//
//  Label.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import UIKit

extension UILabel {
    func createLabel (title: String, height: Int, tag: Int, size: Int, font: String, align: NSTextAlignment) -> UILabel {
        let label = UILabel()
       
        label.text = title
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.textColor = UIColor.black
        label.textAlignment = align
        label.font = UIFont(name: font, size: CGFloat(size))
        label.alpha = 1.0
        label.tag = tag
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(height)).isActive = true
        label.numberOfLines = 10
        label.isUserInteractionEnabled = true
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        return label
    }
}
