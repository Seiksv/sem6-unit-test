//
//  ImageBuilder.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import UIKit

extension UIImageView {
    func createImageView (container: UIView) -> UIImageView {
    
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 100, height: container.frame.height/2))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: CGFloat(400)).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: container.bounds.width).isActive = true

    return imageView
    }
}
