//
//  Row.swift
//  sem6 maoo
//
//  Created by Seiks on 11/3/22.
//

import Foundation
import UIKit
extension UIView {
    func createRow (title: String, height: Int, tag: Int, size: Int, font: String, align: NSTextAlignment, view: UIView) -> UIStackView {
        let stackview = UIStackView().createStackView(distribution: .fill, axis: .vertical, spacing: 10)
        let label = UILabel().createLabel(title: title, height: 20, tag: 0, size: 12, font: FontConstants.hiraginoFont.rawValue, align: .left)
        
        
        stackview.addSubview(label)
        
        let line = UIView()
        label.addSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        line.widthAnchor.constraint(equalToConstant: label.bounds.width - 40).isActive = true
        line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        line.leftAnchor.constraint(equalTo: label.leftAnchor, constant: 20).isActive = true
        line.topAnchor.constraint(equalTo: label.bottomAnchor,constant: 20).isActive = true
        line.backgroundColor = .gray
        
        stackview.addSubview(line)
        return stackview
    }
}
