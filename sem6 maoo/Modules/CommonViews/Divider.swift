//
//  Divider.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import UIKit

extension UIView {
    func addLine(stackView: UIStackView, afterOf: UIView) {
        let line = UIView()
        stackView.addArrangedSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        line.widthAnchor.constraint(equalToConstant: stackView.bounds.width - 40).isActive = true
        line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        line.leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: 20).isActive = true
        line.topAnchor.constraint(equalTo: afterOf.bottomAnchor,constant: 20).isActive = true
        line.backgroundColor = .gray
    }
}
