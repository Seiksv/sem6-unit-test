//
//  TableView.swift
//  sem6 maoo
//
//  Created by Seiks on 12/3/22.
//

import Foundation
import UIKit
extension UITableView {
    func buildTableView (){
        let tableView = UITableView()
        tableView.register(UITableView.self, forCellReuseIdentifier: "cell")
        
    }
}



extension UIView {
    func pin (to superView: UIView){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        self.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
    }
    func pinWithMargin (to superView: UIView){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leftAnchor.constraint(equalTo: superView.leftAnchor, constant: 30).isActive = true
        self.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: superView.rightAnchor, constant: 30).isActive = true
        self.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
    }
}
