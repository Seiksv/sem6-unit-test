//
//  StackView.swift
//  sem6 maoo
//
//  Created by Seiks on 13/3/22.
//

import Foundation
import UIKit

extension UIStackView {
    func createStackView(distribution: UIStackView.Distribution, axis: NSLayoutConstraint.Axis, spacing: Int) -> UIStackView {
        let stackView = UIStackView()
        let tap = UITapGestureRecognizer()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = distribution
        stackView.axis = axis
        stackView.addGestureRecognizer(tap)
        stackView.isUserInteractionEnabled = true
        stackView.spacing = CGFloat(spacing)
        
        return stackView
    }
}
 
