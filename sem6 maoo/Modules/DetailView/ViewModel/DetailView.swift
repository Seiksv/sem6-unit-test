//
//  DetailView.swift
//  sem6 maoo
//
//  Created by Seiks on 12/3/22.
//

import Foundation
import UIKit
import PromiseKit
import Alamofire

class DetailView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var series: Series?
    var amiiboType: AmiiboType?
    var amiiboSeriesDetail: [SeriesDetail] = []
    var amiiboSeriesDetailFiltered: [SeriesDetail] = []
    var networkStatus: NetworkStatus? = nil
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true }
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let serie = series, let amiiboType = amiiboType{
            title = serie.name
            print(serie.key)
            
            networkStatus = NetworkStatus(controllerP: self)
            networkStatus?.buildNotifier()
            
            fetchData(data: serie, amiiboType: amiiboType)
            configSearch()
        } else {
            print("no data :c in serie")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let seriesDetail: SeriesDetail
        
        guard
            segue.identifier == "ShowAmiiboSegue",
            let indexPath = tableView.indexPathForSelectedRow,
            let detailViewController = segue.destination as? AmiiboDetail
        else {
            return
        }
        
        if isFiltering {
            seriesDetail = amiiboSeriesDetailFiltered[indexPath.row]
        } else {
            seriesDetail = amiiboSeriesDetail[indexPath.row]
        }
        
        detailViewController.dataInfo = seriesDetail
    }
    
    func fetchData(data: Series, amiiboType :AmiiboType){
        var request: APIRouter
        var seriesManager: SeriesDetailCacheManager
        
        if amiiboType == .gameseries {
            request = APIRouter.getAmibosSeries(id: data.key!)
            seriesManager = SeriesDetailCacheManager(cacheKey: "\(AmiiboType.amiiboserie)\(data.key!)")
        } else {
            request = APIRouter.getAmibosGameSeries(id: data.key!)
            seriesManager = SeriesDetailCacheManager(cacheKey: "\(AmiiboType.gameseries)\(data.key!)")
        }
        
        self.amiiboSeriesDetail.removeAll()
        self.amiiboSeriesDetailFiltered.removeAll()
        
        if let network = networkStatus, network.getConectionStatus() {
            
            let session = Session.default
            session.request(request)
                .done { (payload: AmiiboSeries) in
                    print("\(payload)")
                  
                    self.amiiboSeriesDetailFiltered = payload.seriesDetail
                    self.amiiboSeriesDetail = payload.seriesDetail
                    
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    try? seriesManager.set(series: payload.seriesDetail)

                }.catch { error in
                    print(error)
                }
                .finally {
                    print("done")
                }
        } else {
            print("No internet ")
            if let serieCache = seriesManager.getSeries()  {
                    //loader.stopAnimating()
                self.amiiboSeriesDetailFiltered = serieCache
                self.tableView.dataSource = self
                self.amiiboSeriesDetail = serieCache
                self.tableView.reloadData()
                
                Toast.show(message: "Contenido cargado desde cache", controller: self)
            } else {
                Toast.show(message: "No hay contenido guardado desde cache", controller: self)
            }
        }
    }
    
    func configSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Series"
        navigationItem.searchController = self.searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func filterContentForSearchText(_ searchText: String) {
        amiiboSeriesDetailFiltered = amiiboSeriesDetail.filter { (seriesDetail: SeriesDetail) -> Bool in
            if isSearchBarEmpty {
                return true
            } else {
                return  seriesDetail.name!.lowercased().contains(searchText.lowercased())
            }
        }
        tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
}


extension DetailView: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            searchFooter.setIsFilteringToShow(filteredItemCount:
                                                amiiboSeriesDetailFiltered.count, of: amiiboSeriesDetail.count)
            return amiiboSeriesDetailFiltered.count
        }
        
        searchFooter.setNotFiltering()
        return amiiboSeriesDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! seriesTableViewCell
        let amiibo: SeriesDetail
        
         if isFiltering {
             amiibo = amiiboSeriesDetailFiltered[indexPath.row]
         } else {
             amiibo = amiiboSeriesDetail[indexPath.row]
         }
        
        cell.gameLabel.text = amiibo.name
        if let image = amiibo.image {
            
            let imageProcess = ImageHelper().getImage(imageURL: URL(string: image)!)
            imageProcess
                .then({ proccessingData -> Promise<UIImage> in
                   // self.activityLoader.stopAnimating()
                    return Promise.value(proccessingData)
                })
                .done({ icon in
                    cell.gameImage.image = icon
                })
                .catch({ error in
                    print(error)
                })
        } else {
            cell.gameImage.image = UIImage(named: "noImage")
            Toast.show(message: "No imagen", controller: self)
            print("no imagen en api")
        }
        
        return cell
    }
}


extension DetailView: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

extension DetailView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}
